# TODO Management APP

[![Built with Spacemacs](https://cdn.rawgit.com/syl20bnr/spacemacs/442d025779da2f62fc86c2082703697714db6514/assets/spacemacs-badge.svg)](http://spacemacs.org)

A simple application for the use of TODO management, like a scheduling system for to develop projects, with the basic funcionalites.


## Designed by:
Daissi Bibiana Gonzalez Roldan
**E-Mail:** dbgroldan@gmail.com

### How to use this project

1. **Clone this Repo**
  ```sh
  → git clone https://gitlab.com/dbRoldan/ror_todo_management.git
  ```
2. **Build the Dockerfile**
  ```sh
  → docker-compose build
  ```

3. **If there were no errors in the previous step**
```sh
  → docker-compose up
  ```

4. **Enter the web page, on the routes:**

[127.0.0.1:3000](http://127.0.0.1:3000 "Link a Pagina")

[localhost:3000](http://localhost:3000/ "Link a Pagina")

**Note:** The website will work in test mode.

#### Used technologies
The technologies that were used are:

* [Ruby on Rails](https://rubyonrails.org/ "ror link")

Rails 5.2.1

![Ruby on Rails](app/assets/images/rails.png " Ruby On Rails")


* [Sqlite 3](https://sqlite.org/index.html "squlite link")

Sqlite 3.25.3

![Sqlite 3](app/assets/images/sqlite3.png " Sqlite 3")

* [Docker](https://www.docker.com/ "docker link")

Docker version 18.09.0-ce, build 4d60db472b

![Docker](app/assets/images/docker.png " Docker")

* [Boostrap](http://getbootstrap.com/ "boostrap link")

Bootstrap v 4.1.3'

![Boostrap](app/assets/images/boostrap.png " Boostrap")
### Licence
GNU-GPL 3.0
