class CreateTables < ActiveRecord::Migration[5.2]
  def change
    create_table :tables do |t|
      t.string :title
      t.text :description
      t.integer :state
      t.date :creation_date

      t.timestamps
    end
  end
end
