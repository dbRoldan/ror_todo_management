class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.string :title
      t.text :details
      t.date :start_date
      t.date :finish_date
      t.integer :state
      t.references :table, foreign_key: true

      t.timestamps
    end
  end
end
