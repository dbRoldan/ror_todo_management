class Task < ApplicationRecord
  belongs_to :table
  validates :title, presence: true,
                    length: { minimum: 3 }
  validate :is_valid_date?

  private

  def is_valid_date?
    if Date.today> finish_date || start_date>finish_date || table.creation_date > start_date
      errors.add(:finish_date, 'invalid date')
    end
  end

end
