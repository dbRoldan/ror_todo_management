class TasksController < ApplicationController
  def edit
    @table = Table.find(params[:table_id])
    @task = @table.tasks.find(params[:id])
  end

  def create
    @table = Table.find(params[:table_id])
    @task = @table.tasks.create(task_params)
    redirect_to table_path(@table)
  end

  def destroy
    @table = Table.find(params[:table_id])
    @task = @table.tasks.find(params[:id])
    @task.destroy
    redirect_to table_path(@table)
  end

  def update
   @table = Table.find(params[:table_id])
   @task = @table.tasks.find(params[:id])
   if @task.update(task_params)
     redirect_to table_path(@table)
   else
     render 'edit'
   end
 end

  private
    def task_params
      params.require(:task).permit(:title, :details, :start_date, :finish_date, :state)
    end
end
